import config.ApplicationContextConfig;
import domain.Attribute;
import domain.Customer;
import domain.Inquiry;
import domain.Topic;
import exception.ServiceException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import service.CallCenterService;
import web.CallCenterController;

import javax.persistence.NoResultException;
import java.util.Set;

/**
 * Created by Евгений on 18.02.2017.
 */


public class CallCenterTest {
    private static CallCenterService callCenterService;


    @BeforeClass
    public static void init(){
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationContextConfig.class);
        callCenterService = context.getBean(CallCenterService.class);
    }

    @Test
    public void createTest() throws ServiceException {

        Topic topic = new Topic();
        topic.setTopicId(1);

        Attribute attribute = new Attribute();;
        attribute.setName("test");
        attribute.setValue("test");

        Customer customer = new Customer();
        customer.setCustomerName("testOne");

        Inquiry inquiry = new Inquiry();
        inquiry.setInquiryName("testCreate");
        inquiry.setTopic(topic);
        inquiry.setCustomer(customer);

        inquiry.addAttributes(attribute);
        Inquiry inquiryGet = callCenterService.createInquiry(inquiry);

        Assert.assertTrue(inquiryGet.getCustomer().getCustomerName().equals(inquiry.getCustomer().getCustomerName()));
        Assert.assertTrue(inquiryGet.getTopic().getTopicId() == inquiry.getTopic().getTopicId());
        Assert.assertTrue(inquiryGet.getInquiryName().equals(inquiry.getInquiryName()));
        Assert.assertTrue(inquiryGet.getAttributes().size() == inquiry.getAttributes().size());

        for (Attribute element : inquiryGet.getAttributes()){
            Assert.assertTrue(element.getName().equals(attribute.getName()));
            Assert.assertTrue(element.getValue().equals(attribute.getValue()));
        }
        callCenterService.deleteInquiry("Alexei", inquiryGet.getInquiryId());

    }

    @Test(expected = NoResultException.class)

    public void deleteTest() throws ServiceException {
        Topic topic = new Topic();
        topic.setTopicId(1);

        Attribute attribute = new Attribute();;
        attribute.setName("test");
        attribute.setValue("test");

        Customer customer = new Customer();
        customer.setCustomerName("testOne");

        Inquiry inquiry = new Inquiry();

        inquiry.setInquiryName("testCreate");
        inquiry.setTopic(topic);
        inquiry.setCustomer(customer);
        inquiry.addAttributes(attribute);

        Inquiry inquiryGet = callCenterService.createInquiry(inquiry);

        callCenterService.deleteInquiry("testOne",inquiryGet.getInquiryId());
        Assert.assertNull(callCenterService.getInquiryOfCustomer("testOne",inquiryGet.getInquiryId()));
    }
    @Test
    public void testUpdate() throws ServiceException {

        Inquiry inquiry = callCenterService.getInquiryOfCustomer("testOne",1);


        Topic topicUpdate = new Topic();
        topicUpdate.setTopicId(2);


        inquiry.setInquiryName("update");
        inquiry.setTopic(topicUpdate);




        callCenterService.updateInquiry("testOne", inquiry, 1);

        Inquiry inquiryGet = callCenterService.getInquiryOfCustomer("testOne",1);
        Assert.assertTrue(inquiryGet.getInquiryName().equals("update"));
        Assert.assertTrue(inquiryGet.getTopic().getTopicName().equals("testTwo"));
        Assert.assertTrue(inquiryGet.getCustomer().getCustomerName().equals("testOne"));

        inquiry.setInquiryName("testOne");
        topicUpdate.setTopicId(1);
        inquiry.setTopic(topicUpdate);
        callCenterService.updateInquiry("testOne",inquiry,1);


    }

    @Test
    public void getAllTopicsTest() throws ServiceException {
        Set<Topic> set = callCenterService.getTopics();
        Assert.assertTrue(set.size() > 0);
    }

    @Test
    public void getInquiriesByCustomer() throws ServiceException {
        Set<Inquiry> set = callCenterService.getSetInquiries("testOne");
        System.out.println(set.size());
        Assert.assertTrue(set.size() > 0);
    }

    @Test
    public void getInquiryByCustomer() throws ServiceException {
        Inquiry inquiry = callCenterService.getInquiryOfCustomer("testTwo",2);
        Assert.assertTrue(inquiry.getInquiryName().equals("testTwo"));
        Assert.assertTrue(inquiry.getTopic().getTopicId() == 2);
    }
}
