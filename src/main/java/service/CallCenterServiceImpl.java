package service;

import dao.CallCenterDAO;
import domain.Attribute;
import domain.Customer;
import domain.Inquiry;
import domain.Topic;
import exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.Set;

/**
 * Created by Евгений on 07.02.2017.
 */
@Service
public class CallCenterServiceImpl implements CallCenterService {
    @Autowired
    private CallCenterDAO callCenterDAO;

    @Transactional
    @Override
    public Set<Topic> getTopics() throws ServiceException {
        try {
            return callCenterDAO.getAllTopics();
        } catch (SQLException e) {
            throw new ServiceException(e);
        }
    }

    @Transactional
    @Override
    public Set<Inquiry> getSetInquiries(String customerName) throws ServiceException {
        Set<Inquiry> set;
        try {
            set = callCenterDAO.getInquiriesByCustomer(callCenterDAO.getCustomerByName(customerName));
        } catch (SQLException e) {
            throw new ServiceException(e);
        }
        return set;

    }

    @Transactional
    @Override
    public Inquiry getInquiryOfCustomer(String customerName, Integer inquiryId) throws ServiceException {
        Inquiry inquiry;
        try {
            inquiry = callCenterDAO.getOneInquiryByCustomer(callCenterDAO.getCustomerByName(customerName),inquiryId);
        } catch (SQLException e) {
            throw new ServiceException(e);
        }
        return inquiry;
    }

    @Transactional
    @Override
    public Inquiry createInquiry(Inquiry inquiry) throws ServiceException {
        String customerName = inquiry.getCustomer().getCustomerName();
        Customer customer;
        Inquiry inquiryGet;
        try {
            if (callCenterDAO.isCustomerExist(customerName)) {
                customer = callCenterDAO.getCustomerByName(customerName);
            }
            else {
                customer = new Customer();
                customer.setCustomerName(customerName);
            }
            inquiry.setCustomer(customer);
            inquiryGet = callCenterDAO.createInquiry(inquiry);
        } catch (SQLException e) {
            throw new ServiceException(e);
        }
        return inquiryGet;
    }

    @Transactional
    @Override
    public void updateInquiry(String customerName, Inquiry inquiry, int inquiryId) throws ServiceException {
        Customer customer = new Customer();
        customer.setCustomerName(customerName);
        inquiry.setCustomer(customer);

        Inquiry inquiryUpd;

        inquiryUpd = getInquiryOfCustomer(customerName,inquiryId);
        inquiryUpd.setInquiryName(inquiry.getInquiryName());
        inquiryUpd.setTopic(inquiry.getTopic());
        inquiryUpd.setAttributes(inquiry.getAttributes());
        for (Attribute element: inquiryUpd.getAttributes()){
            inquiryUpd.addAttributes(element);
        }

    }


    @Transactional
    @Override
    public void deleteInquiry(String customerName, Integer inquiryId) throws ServiceException {

        try {
            callCenterDAO.deleteInquiry(inquiryId);
        } catch (SQLException e) {
            throw new ServiceException(e);
        }
    }




}
