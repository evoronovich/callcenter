package service;

import domain.Inquiry;
import domain.Topic;
import exception.ServiceException;

import java.util.Set;

/**
 * Created by Евгений on 10.02.2017.
 */
public interface CallCenterService {
    Set<Topic> getTopics() throws ServiceException;
    Set<Inquiry> getSetInquiries(String customerName) throws ServiceException;
    Inquiry getInquiryOfCustomer(String customerName, Integer inquiryId) throws ServiceException;
    Inquiry createInquiry(Inquiry inquiry) throws ServiceException;
    void updateInquiry(String customerName, Inquiry inquiry, int inquiryId) throws ServiceException;
    void deleteInquiry(String customerName, Integer inquiryId) throws ServiceException;

}
