package exception;

/**
 * Created by Евгений on 15.03.2017.
 */
public class ServiceException extends RuntimeException {

    public ServiceException(Exception e){
        super(e);
    }
}
