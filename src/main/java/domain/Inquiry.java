package domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Евгений on 09.02.2017.
 */
@Entity
public class Inquiry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "inquiryId", nullable = false)
    private int inquiryId;
    @Column(name = "name", length = 45)
    private String inquiryName;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "customerId")
    private Customer customer;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "topicId")
    private Topic topic;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "inquiry", cascade = CascadeType.ALL)
    private  Set<Attribute>  attributes = new HashSet<>();

    public  Set<Attribute> getAttributes() {
        return attributes;
    }

    public void addAttributes(Attribute attribute){
        this.getAttributes().add(attribute);
        attribute.setInquiry(this);
    }

    public  void setAttributes(Set<Attribute> attributes) {
        for (Attribute entry : attributes){
            entry.setInquiry(this);
        }
        this.attributes = attributes;
    }

    public int getInquiryId() {
        return inquiryId;
    }

    public void setInquiryId(int inquiryId) {
        this.inquiryId = inquiryId;
    }


    public String getInquiryName() {
        return inquiryName;
    }

    public void setInquiryName(String name) {
        this.inquiryName = name;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Inquiry inquiry = (Inquiry) o;

        if (inquiryId != inquiry.inquiryId) return false;
        if (inquiryName != null ? !inquiryName.equals(inquiry.inquiryName) : inquiry.inquiryName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = inquiryId;
        result = 31 * result + (inquiryName != null ? inquiryName.hashCode() : 0);

        return result;
    }


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customerByCustomerId) {
        this.customer = customerByCustomerId;
    }


    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topicByTopicId) {
        this.topic = topicByTopicId;
    }
}
