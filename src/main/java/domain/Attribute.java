package domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by Евгений on 16.02.2017.
 */
@Entity
public class Attribute {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "AttributeId", nullable = false)
    private Integer AttributeId;
    @Column(name = "name", nullable = true, length = 45)
    private String name;
    @Column(name = "value", nullable = true, length = 45)
    private String value;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "inquiryId")
    @JsonIgnore
    private Inquiry inquiry;

    public Inquiry getInquiry() {
        return inquiry;
    }

    public void setInquiry(Inquiry inquiry) {
        this.inquiry = inquiry;
    }

    public Integer getAttributeId() {
        return AttributeId;
    }

    public String getName() {
        return name;
    }


    public void setAttributeId(Integer attributeId) {
        AttributeId = attributeId;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
