package web.dto;

import domain.Attribute;
import domain.Customer;
import domain.Topic;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Евгений on 16.03.2017.
 */
public class InquiryForm {

    private int inquiryId;
    private String name;
    private Customer customer;
    private Topic topic;
    private List<Attribute> attributes = new ArrayList<>();
    private int number = 0;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getInquiryId() {
        return inquiryId;
    }

    public void setInquiryId(int inquiryId) {
        this.inquiryId = inquiryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }
}
