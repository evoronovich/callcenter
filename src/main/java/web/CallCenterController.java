package web;


import domain.Attribute;
import domain.Customer;
import domain.Inquiry;
import domain.Topic;
import exception.ServiceException;
import org.osgi.service.useradmin.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.util.UriComponentsBuilder;
import service.CallCenterService;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Евгений on 27.02.2017.
 */
@RestController
@EnableWebMvc
@SessionAttributes(value = "inquiryForm")
public class CallCenterController {


    @Autowired
    CallCenterService service;
    @RequestMapping({"/","home"})
    public String showHomePage(Model model){
        return "home";
    }
    /*---------FORM EXAMPLES------------
    @RequestMapping(value = "newInquiry", method = RequestMethod.GET)
    public String  newInquiry(Model model)  {

        if (!model.containsAttribute("inquiryForm")) {
            InquiryForm inquiryForm = new InquiryForm();
            model.addAttribute("inquiryForm", inquiryForm);
        }
        try {
            model.addAttribute("list",service.getTopics());
        } catch (ServiceException e) {
            e.printStackTrace();
            return "error";
        }
        return "newInquiry";
    }
    @RequestMapping(value = "newInquiry", method = RequestMethod.POST)
    public String addInquiryFromForm(@Validated InquiryForm inquiryForm)  {
        Inquiry inquiry = new Inquiry();
        inquiry.setName(inquiryForm.getName());
        inquiry.setCustomer(inquiryForm.getCustomer());
        inquiry.setTopic(inquiryForm.getTopic());
        for (Attribute element : inquiryForm.getAttributes()){
            inquiry.addAttributes(element);
        }
        try {
            service.createInquiry(inquiry);
        }
         catch (ServiceException e) {
             e.printStackTrace();
             return "error";
        }


        return "redirect:/home";

        @RequestMapping(value = "addAttr", method = RequestMethod.GET)
    public String addAttr(InquiryForm inquiryForm){
        int counter = inquiryForm.getNumber();
        inquiryForm.setNumber(counter++);
        return "redirect:/newInquiry";
    }

    }---------------------*/

    @RequestMapping(value = "/topics", method = RequestMethod.GET)
    public  Set<Topic>  getTopics(){
        Set<Topic> topics = new HashSet<>();
        try {
            topics = service.getTopics();
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        return topics;
    }
    @RequestMapping(value = "/customers/{customerName}/inquiries", method = RequestMethod.GET)
    public Set<Inquiry>  getInquiriesOfCustomers(@PathVariable String customerName){
        Set<Inquiry> inquiries = new HashSet<>();
        try {
            inquiries = service.getSetInquiries(customerName);
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        return inquiries;
    }

    @RequestMapping(value = "/customers/{customerName}/inquiries/{inquiryId}", method = RequestMethod.GET)
    public Inquiry  getInquiryOfCustomers(@PathVariable String customerName, @PathVariable int inquiryId){
        Inquiry inquiry = new Inquiry();
        try {
            inquiry = service.getInquiryOfCustomer(customerName,inquiryId);
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        return inquiry;
    }

    @RequestMapping(value = "/customers/{customerName}/inquiries", method = RequestMethod.POST)
    public ResponseEntity<Inquiry> addInquiryRest(@PathVariable String customerName, @RequestBody Inquiry inquiry)   {
        Customer customer = new Customer();
        customer.setCustomerName(customerName);
        inquiry.setCustomer(customer);
        for (Attribute element: inquiry.getAttributes()){
            element.setInquiry(inquiry);
        }
        try {
            service.createInquiry(inquiry);
        }
        catch (ServiceException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<Inquiry>(inquiry, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/customers/{customerName}/inquiries/{inquiryId}", method = RequestMethod.PUT)
    public ResponseEntity<Inquiry> updateInquiryRest(@PathVariable String customerName, @PathVariable int inquiryId,
                                                  @RequestBody Inquiry inquiry)   {
        try {
            service.updateInquiry(customerName, inquiry, inquiryId);
        }

        catch (ServiceException e) {
            e.printStackTrace();
            return new ResponseEntity<Inquiry>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Inquiry>(HttpStatus.OK);
    }



    @RequestMapping(value = "/customers/{customerName}/inquiries/{inquiryId}", method = RequestMethod.DELETE)
    public ResponseEntity<Inquiry> deleteInquiry(@PathVariable String customerName, @PathVariable int inquiryId)  {
        try {
            service.deleteInquiry(customerName,inquiryId);
        }
        catch (ServiceException e) {
            e.printStackTrace();
            return new ResponseEntity<Inquiry>(HttpStatus.NOT_FOUND);

        }
        return new ResponseEntity<Inquiry>(HttpStatus.OK);

    }





}
