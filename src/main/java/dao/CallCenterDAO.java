package dao;

import domain.Customer;
import domain.Inquiry;
import domain.Topic;

import java.sql.SQLException;
import java.util.Set;

/**
 * Created by Евгений on 08.02.2017.
 */
public interface CallCenterDAO {
    Set<Inquiry> getInquiriesByCustomer(Customer customer) throws SQLException;
    Inquiry getOneInquiryByCustomer(Customer customer, Integer inquiryId) throws SQLException;
    Set<Topic> getAllTopics() throws SQLException;
    Inquiry createInquiry(Inquiry inquiry) throws SQLException;
    void deleteInquiry(Integer inquiryId) throws SQLException;
    Customer getCustomerByName(String name) throws SQLException;
    boolean isCustomerExist(String name) throws SQLException;

}
