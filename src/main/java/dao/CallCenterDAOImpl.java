package dao;

import domain.Customer;
import domain.Inquiry;
import domain.Topic;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Евгений on 08.02.2017.
 */
@Repository
public class CallCenterDAOImpl implements CallCenterDAO {

    @PersistenceContext(name = "CCPersistenceUnit")
    private EntityManager entityManager;

    @Override
    public Set<Inquiry> getInquiriesByCustomer(Customer customer) throws SQLException {
        Set<Inquiry> inquiries = new HashSet<>();

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Inquiry> cq = cb.createQuery(Inquiry.class);
        Root<Inquiry> root = cq.from(Inquiry.class);
        cq.where(cb.equal(root.get("customer"),customer));
        TypedQuery<Inquiry> inquiryTypedQuery = entityManager.createQuery(cq);

        inquiries.addAll(inquiryTypedQuery.getResultList());


        /*inquiries.addAll(entityManager.createQuery
                ("select i from Inquiry i where i.customer LIKE :custGet").setParameter
                ("custGet",customer).getResultList());*/


        return inquiries;
    }

    @Override
    public Inquiry getOneInquiryByCustomer(Customer customer, Integer inquiryId) throws SQLException {
        Inquiry result;
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Inquiry> cq = cb.createQuery(Inquiry.class);
        Root<Inquiry> root = cq.from(Inquiry.class);
        cq.where(cb.equal(root.get("customer"),customer));
        cq.where(cb.equal(root.get("inquiryId"),inquiryId));
        TypedQuery<Inquiry> inquiryTypedQuery = entityManager.createQuery(cq);
        result = inquiryTypedQuery.getSingleResult();

        /*result = (Inquiry) entityManager.createQuery
                ("select i from Inquiry i where i.customer like :custGet and i.inquiryId like :inqIdGet").setParameter
                ("custGet",customer).setParameter("inqIdGet",inquiryId).getSingleResult();*/
        return result;
    }


    @Override
    public Set<Topic> getAllTopics() throws SQLException {

        Set<Topic> topics = new HashSet<>();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Topic> cq = cb.createQuery(Topic.class);
        Root<Topic> root = cq.from(Topic.class);
        cq.select(root);
        TypedQuery<Topic> topicTypedQuery = entityManager.createQuery(cq);
        topics.addAll(topicTypedQuery.getResultList());


        return topics;
    }


    @Override
    public Inquiry createInquiry(Inquiry inquiry) throws SQLException {
        Inquiry inquiryGet;
        inquiryGet = (Inquiry) entityManager.merge(inquiry);
        return inquiryGet;
    }

    @Override
    public void deleteInquiry(Integer inquiryId) throws SQLException {
        Inquiry inquiry = entityManager.find(Inquiry.class,inquiryId);
        entityManager.remove(inquiry);
    }

    @Override
    public Customer getCustomerByName(String name) throws SQLException {
        Customer result;
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Customer> cq = cb.createQuery(Customer.class);
        Root<Customer> root = cq.from(Customer.class);
        cq.where(cb.equal(root.get("customerName"),name));
        TypedQuery<Customer> inquiryTypedQuery = entityManager.createQuery(cq);
        result = inquiryTypedQuery.getSingleResult();

        /*result = (Customer) entityManager.createQuery("select c from Customer c where c.customerName LIKE :custName").setParameter("custName",name).getSingleResult();*/

        return result;
    }

    @Override
    public boolean isCustomerExist(String name) throws SQLException {

        boolean result;
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Customer> cq = cb.createQuery(Customer.class);
        Root<Customer> root = cq.from(Customer.class);
        cq.where(cb.equal(root.get("customerName"),name));
        TypedQuery<Customer> inquiryTypedQuery = entityManager.createQuery(cq);
        result = inquiryTypedQuery.getResultList().size() != 0;

        /*result = entityManager.createQuery("select c from Customer c where c.customerName LIKE :custName").setParameter("custName",name).getResultList().size() != 0;*/

        return result;
    }




}
