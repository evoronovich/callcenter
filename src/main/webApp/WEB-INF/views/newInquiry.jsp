<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


    <h2>Create a new Inquiry</h2>

    <form:form action="newInquiry" id="spr" method="POST" modelAttribute="inquiryForm">
        <fieldset>
        <table cellspacing="0">

                <tr>
                    <th><label for="name">Inquiry name:</label></th>
                    <td><form:input path="name" size="15" id="name"/></td>
                </tr>
                <tr>
                    <th><label for="CustomerName">Customer name:</label></th>
                    <td><form:input path="Customer.CustomerName" size="15" id="CustomerName"/></td>
                </tr>
                <tr>
                    <td>Topic</td>
                    <td><form:select path="Topic.topicId">
                        <form:option value="" label="Select Topic" />
                        <form:options items="${list}" itemLabel="topicName" itemValue="topicId" />
                    </form:select></td>
                </tr>

            
            <c:forEach begin="1" end="${inquiryForm.number}" varStatus="status">
                <tr>
                    <th><label for="attrName">${status.index} Attribute name:</label></th>
                    <td><form:input path="attributes[${status.index}].name" size="15" id="attrName"/></td>
                    <th><label for="attrValue">${status.index} Attribute value:</label></th>
                    <td><form:input path="attributes[${status.index}].value" size="15" id="attrValue"/></td>
                </tr>
            </c:forEach>




            <tr>

    </table>
        </fieldset>
        <form:button>Submit</form:button>
    </form:form>
<form action="/addAttr">
    <input type="submit" value="Add Attribute" align="top"/>
</form>

