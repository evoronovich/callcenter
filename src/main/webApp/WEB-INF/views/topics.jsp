<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Евгений
  Date: 15.03.2017
  Time: 21:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Списки тем</title>
</head>
<body>
<table border="1px">
    <c:forEach items="${topics}" var="topic">
        <tr>
            <td>${topic.topicName}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
