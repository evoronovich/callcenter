## Call center test Application ##


An application of handling customer inquiries to the call center.

The application can implement the following scenarios:

Adding inquiry:

1. The customer contacts technical support with any problem, for example, the communication does not work.
2. The operator creates an inquiry on one of the topics, selecting the most suitable directory. Additional attributes may be specified in the call, for example, the address, phone model, city district, etc.
3. The operator adds an inquiry to the database.

Updating inquiry:

1. The customer contacts again to clarify something.
2. The operator finds a registered customer inquiry.
3. The operator updates the inquiry.

Deleting inquiry:

1. Customer applies that problem is solved.
2. The operator finds a registered customer inquiry.
3. The operator deletes the inquiry.

It is RESTful web-service for Tomcat, that supports next operations:

* Get list of possible topics: GET / topics 
* Get list of customer inquiries: GET /customers/{customerName}/inquiries 
* Get one customer inquiry: GET /customers/{customerName}/inquiries/{inquiryId} 
* Create customer inquiry: POST /customers/{customerName}/inquiries 
* Update customer inquiry: PUT /customers/{customerName}/inquiries/{inquiryId} 
* Delete customer inquiry: DEL / customers/{customerName}/inquiries/{inquiryId} 

Voice: +375298543875

E-mail: zhenek.zik@gmail.com